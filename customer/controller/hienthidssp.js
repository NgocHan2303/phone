export let hienthisp = (sp) => {
  let contentHTML = " ";
  sp.forEach((item) => {
    //

    //
    // type :true (apple) ;false (samsung)
    // mota: true (còn hàng) ;false (hết hàng)
    let content = `
<div class="col-lg-4 col-md-6 d-flex">
                <div class="container row">
                  <div class="card text-black h-100">
                    <div class="content-overlay"></div>
                    <img src="./sp/${
                      item.id
                    }.jpg" class="card-img" alt="Phone Image" />
                    <div class="content-details fadeIn-top">
                      <h3 class="pb-5">Specifications</h3>
                      <div class="d-flex justify-content-start py-1">
                        <span class="text-light"><b>Screen:</b></span>
                        <span class="text-light">&nbsp; 1</span>
                      </div>
                      <div class="d-flex justify-content-start py-1">
                        <span class="text-light"><b>Back Camera:</b> 1</span>
                      </div>
                      <div class="d-flex justify-content-start py-1">
                        <span class="text-light"><b>Front Camera:</b> 1</span>
                      </div>

                      <p class="pt-5"><u>click here for more details</u></p>
                    </div>
                    <div class="card-body">
                      <div class="text-center">
                        <h5 class="card-title pt-3">3</h5>
                        <span class="text-muted mb-2">$1</span>
                        <span class="text-danger"><s>$301</s></span>
                      </div>
                      <div class="mt-3 brand-box text-center">
                        <span> <td>${item.type ? "Apple" : "Samsung"}</td> 
                        
                        </span>
                      </div>
                      <div class="d-flex justify-content-start pt-3">
                        <span><b>Description:</b> 1</span>
                      </div ><td>${
                        item.moTa ? "Còn Hàng" : "Hết Hàng"
                      }</td></div>
                      <div>
                      <div class="d-flex justify-content-between pt-3">
                        <div class="text-warning">
                          <i class="fa fa-star"></i>
                          <i class="fa fa-star"></i>
                          <i class="fa fa-star"></i>
                          <i class="fa fa-star"></i>
                          <i class="fa fa-star"></i>
                        </div>
                        <span class="text-success"><b>In Stock</b></span>
                      </div>
                      <button
                        type="button"
                        class="btn btn-block w-50"
                        onclick="btnAddToCart(${item.id})"
                      >
                        Add to cart
                      </button>
                    </div>
                  </div>
                </div>
               
                
              </div>


`;
    contentHTML += content;
  });
  document.getElementById("phoneList").innerHTML = contentHTML;
};

export let onSuccess = (message) => {
  Toastify({
    text: message,
    duration: 3000,
    destination: "https://github.com/apvarun/toastify-js",
    newWindow: true,
    close: true,
    gravity: "top", // `top` or `bottom`
    position: "right", // `left`, `center` or `right`
    stopOnFocus: true, // Prevents dismissing of toast on hover
    style: {
      background: "linear-gradient(to right, #00b09b, #96c93d)",
    },
    onClick: function () {}, // Callback after click
  }).showToast();
};
//

// hiển thị giỏ sản phẩm

// customer/view/sp/undefined.jpg

// dinh nghia san phan

export class sanpham {
  constructor(
    id,
    tenSP,
    gia,
    hinhAnh,
    moTa,
    screen,
    blackCamera,
    frontCamera,
    type
  ) {
    id, tenSP, gia, hinhAnh, moTa, screen, blackCamera, frontCamera, type;
  }
}
export class spgiohang {
  constructor(
    id,
    tenSP,
    gia,
    hinhAnh,
    moTa,
    screen,
    blackCamera,
    frontCamera,
    type,
    count
  ) {
    id,
      tenSP,
      gia,
      hinhAnh,
      moTa,
      screen,
      blackCamera,
      frontCamera,
      type,
      count;
  }
}

/*
"tenSP": "tenSP 3",
  "gia": "gia 3",
  "hinhAnh": "hinhAnh 3",
  "moTa": "moTa 3",
  "screen": "screen 3",
  "blackCamera": "blackCamera 3",
  "frontCamera": "frontCamera 3",
  "type": "type 3",
  "id": "3"
<div>
  <td>${item.id}</td>
  <td>${item.tenSP}</td>
  <td>${item.gia}</td>
  <td>${item.screen}</td>
  <td>${item.blackCamera}</td>
  <td>${item.frontCamera}</td>
  <td>${item.hinhAnh}</td>
  <td>${item.moTa}</td>
  <td>${item.type}</td>
  
</div>

*/
