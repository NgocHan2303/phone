import {
  spgiohang,
  sanpham,
  // Giohanghienthi,
  onSuccess,
  hienthisp,
} from "./hienthidssp.js";
// import { dinhnghiamonan } from "./lopdoituong.js";
const base_url = "https://640a899565d3a01f980115a1.mockapi.io";
//
let Giohanghienthi = (spchon) => {
  let tinhtongtiencacsp = (spchon) => {
    let tong = 0;
    spchon.forEach((item) => {
      tong += item.gia * item.count;
    });
    return tong;
  };
  const tienship = tinhtongtiencacsp(spchon) > 0 ? 10 : 0;
  // tổng sản phẩm trong giỏ hàng
  let tinhtongsoluonggiohang = () => {
    let tong = 0;
    spchon.forEach((item) => {
      tong += item.count;
    });
    return tong;
  };
  let thuegtgt = () => {
    let gtgt = (tinhtongtiencacsp(spchon) * 10) / 100;
    return gtgt;
  };
  let contentHTML = " ";
  let tongtien = () => {
    let tongtienphaitra = tinhtongtiencacsp(spchon) + thuegtgt(spchon);

    return tongtienphaitra;
  };

  spchon.forEach((item) => {
    // console.log("item: ", item);
    let content = `
<div class="product">
  <div class="product__1">
    <div class="product__thumbnail">
      <img src=" ./sp/${item.id}.jpg " alt=" tên Sản phẩn">
    </div>
    <div class="product__details">

    <div style="margin-bottom: 8px;"><b>tên sản phẩm: ${item.tenSP}</b></div>
      <div style="margin-bottom: 8px;"><b>Giá: ${item.gia}</b></div>
      <div style="margin-bottom: 8px;"><b>hãng: ${
        item.type ? "Apple" : "Samsung"
      }</b></div>
      <div style="font-size: 90%;">Screen: <span class="tertiary">1</span></div>
      <div style="font-size: 90%;">Back Camera: <span class="tertiary">1</span></div>
      <div style="font-size: 90%;">Front Camera: <span class="tertiary">1</span></div>
      <td>${item.moTa ? "Còn Hàng" : "Hết Hàng"}</td>
      <div style="margin-top: 8px;"><a href="#!" onclick="xoasp('${
        item.id
      }')">Remove</a></div>
    </div>
  </div>
  <div class="product__2">
    <div class="qty">
      <span><b>Quantity:</b> </span> 
      <span class="minus bg-dark" onclick="botsoluong('${item.id}')">-</span>
      <span class="quantityResult mx-2">${item.count}</span>
      <span class="plus bg-dark" onclick="themsoluong('${item.id}')">+</span>
    </div>
    <div class="product__price"><b>${item.count * item.gia}</b></div>
  </div>
</div>

`;
    contentHTML += content;
  });
  document.getElementById("priceTotal").innerHTML = tongtien(spchon);
  document.getElementById("tax").innerHTML = thuegtgt(spchon);
  document.getElementById("subTotal").innerHTML = tinhtongtiencacsp(spchon);
  document.getElementById("cartCount").innerHTML =
    tinhtongsoluonggiohang(spchon);
  tinhtongtiencacsp(spchon) > 0 ? 10 : 0;
  document.getElementById("shipping").innerHTML = "$" + tienship;
  document.getElementById("cartList").innerHTML = contentHTML;
};
const localSpchon = "localSpchon";
// lấy dsmon an từ api
let sp = [];
let dssp = () => {
  axios({ url: `${base_url}/Products`, method: "GET" })
    .then((res) => {
      console.log(res);
      hienthisp(res.data);
      sp = res.data;
    })
    .catch((err) => {
      console.log(err);
    });
};
dssp();
let findItemById = (spchon, id) => {
  let item;
  spchon.forEach((ele) => {
    if (ele.id == id) {
      item = ele;
      return;
    }
  });
  return item;
};
let spchon = [];
let btnAddToCart = (id) => {
  let timspgio = findItemById(spchon, id);
  // console.log("timspgio: ", timspgio);
  if (timspgio == null) {
    let vitri = sp.findIndex(function (item) {
      return item.id == id;
    });
    // console.log("vitri: ", vitri);
    // let gio = new spgiohang(id, 1);
    let addgio = new spgiohang();
    addgio.id = sp[vitri].id;
    addgio.tenSP = sp[vitri].tenSP;
    addgio.gia = sp[vitri].gia;
    addgio.hinhAnh = sp[vitri].hinhAnh;
    addgio.moTa = sp[vitri].moTa;
    addgio.screen = sp[vitri].screen;
    addgio.blackCamera = sp[vitri].blackCamera;
    addgio.frontCamera = sp[vitri].frontCamera;
    addgio.type = sp[vitri].type;
    addgio.count = 1;

    // console.log("addgio: ", addgio);

    spchon.push(addgio);
    // console.log("spchon: ", spchon);
    Giohanghienthi(spchon);

    onSuccess("Thêm giỏ hàng thành công");
    let dataJsong = JSON.stringify(spchon);
    // console.log("dssv: ", dssv);
    // lưu vào localStorage
    localStorage.setItem(localSpchon, dataJsong);
  } else {
    onSuccess("Sản phẩm đã có trong giỏ hàng");
    timspgio.count++;
    Giohanghienthi(spchon);
  }
};

window.btnAddToCart = btnAddToCart;
// hien thi sản phẩm trong giỏ hàng
let jsonData = localStorage.getItem(localSpchon);
if (jsonData != null) {
  spchon = JSON.parse(jsonData);
  Giohanghienthi(spchon);
}

//xóa sản phẩm trong giỏ hàng
window.xoasp = (id) => {
  let vitri = spchon.findIndex(function (item) {
    return item.id == id;
  });

  if (vitri != -1) {
    spchon.splice(vitri, 1);
    Giohanghienthi(spchon);
  }

  onSuccess("xóa giỏ hàng thành công");
  let dataJsong = JSON.stringify(spchon);
  // console.log("dssv: ", dssv);
  // lưu vào localStorage
  localStorage.setItem(localSpchon, dataJsong);
};
//

window.botsoluong = (id) => {
  let vitri = spchon.findIndex(function (item) {
    return item.id == id;
  });
  spchon[vitri].count--;
  if (spchon[vitri].count <= 0) {
    xoasp(id);
  }
  Giohanghienthi(spchon);
  // onSuccess("giảm 1 sản phẩm thành công");
  let dataJsong = JSON.stringify(spchon);
  // console.log("dssv: ", dssv);
  // lưu vào localStorage
  localStorage.setItem(localSpchon, dataJsong);
};
window.themsoluong = (id) => {
  let vitri = spchon.findIndex(function (item) {
    return item.id == id;
  });
  spchon[vitri].count++;
  Giohanghienthi(spchon);
  // onSuccess("thêm 1 sản phẩm thành công");
  let dataJsong = JSON.stringify(spchon);
  // console.log("dssv: ", dssv);
  // lưu vào localStorage
  localStorage.setItem(localSpchon, dataJsong);
};
//

// làm trống giỏ hàng
window.tronggiohang = () => {
  spchon = [];
  Giohanghienthi(spchon);
  let dataJsong = JSON.stringify(spchon);
  localStorage.setItem(localSpchon, dataJsong);
};
window.thanhtoan = () => {
  if (spchon.length > 0) {
    Swal.fire({
      // position: 'top-end',
      icon: "success",
      title: "Your order is completed",
      showConfirmButton: false,
      timer: 1500,
    });

    // onSuccess("Thanh toán thành công");
    tronggiohang();
    let dataJsong = JSON.stringify(spchon);
    localStorage.setItem(localSpchon, dataJsong);
  } else {
    Swal.fire({
      icon: "error",
      title: "Oops...",
      text: "Your cart is empty",
    });
  }
};

//lọc sản phẩm theo hãng
//lọc phone theo hãng

document.getElementById("selectList").onchange = async () => {
  const data = sp;
  const selectValue = document.getElementById("selectList").value;
  let filterData = [];
  if (selectValue == "all") {
    filterData = data;
  } else if (selectValue == "Apple") {
    filterData = data.filter((ele) => ele.type == true);
  } else {
    filterData = data.filter((ele) => ele.type == false);
  }

  hienthisp(filterData);
};
